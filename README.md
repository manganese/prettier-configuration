# Manganese - Prettier Configuration

This is the [Prettier](https://prettier.io) code formatter configuration to use across Manganese JavaScript and TypeScript codebases.

## Installation

To use this configuration in a project, add the following to its `package.json` file:

```json
  "prettier": "@manganese/prettier-configuration",
  "devDependencies": {
    "@manganese/prettier-configuration": "^1.0"
  }
```